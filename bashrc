
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

HISTCONTROL=ignoreboth
HISTTIMEFORMAT='%FT%T%z '
shopt -s histappend
shopt -s checkwinsize


#ALIASES
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
alias ll='ls -lp --group-directories-first --time-style long-iso'
alias lh='ls -lhp --group-directories-first --time-style long-iso'
alias l='ls --group-directories-first'
#alias llt='ls -rtlp --group-directories-first --time-style long-iso'

alias qq='exit'
alias cls='clear'

alias date1='date +%Y%m%d.%H%M%S'
alias date2='date "+%Y-%m-%d %H:%M:%S"'
alias date3='date --rfc-3339 seconds'

alias wh='who --ips'

alias esudo='sudo -E'
alias sdr='screen -d -r'
